# Arcadium - Retro-gaming

![](https://gitlab.com/makerspace-unilasalle/campus-amiens/projets/arcadium-retro-gaming/-/raw/54c8acfa4dcf2f8ff869f952f8dcec09b40b2284/img/img2.png)

## Projet

Ce projet vise à construire une borne d'arcade fonctionnelle à l'aide de technologies telles que la découpeuse laser et l'imprimante 3D. La borne sera exécutée sur un Raspberry Pi et pourra être utilisée pour jouer à de nombreux jeux d'arcade classiques.

## Avancement

La borne a été développé sous [FreeCAD](https://www.freecadweb.org/?lang=fr) est peut être fabriquée au MakerSpace d'UniLaSalle Amiens. Elle fonctionne en utilisant un RaspberryPi sous [recalbox](https://www.recalbox.com/fr/)

## Matériel nécessaire

Pour construire cette borne d'arcade, vous aurez besoin des éléments suivants :

- Un Raspberry Pi
- Un écran
- Des boutons et joysticks
- Un boîtier pour la borne d'arcade (que vous pouvez couper à la découpeuse laser en CP 5mm)
- Une imprimante 3d pour produire les cubes de liaison
- Des fils et connecteurs pour relier tous les éléments ensemble